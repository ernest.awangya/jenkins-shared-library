def call(){
    dir("istio-1.20.0") {
        sh '${ISTIO_PATH}/istioctl analyze'
        sh 'kubectl apply -f samples/addons'
        sh 'kubectl rollout status deployment/kiali -n istio-system'

                        // CLEAN UP
                        // sh 'kubectl delete -f samples/addons', 'Deleting Istio addons failed')
                        // sh '${ISTIO_PATH}/istioctl uninstall -y --purge', 'Istio uninstallation failed')
                        // sh 'kubectl delete namespace istio-system', 'Deleting Istio namespace failed')
                        // sh 'kubectl label namespace default istio-injection-', 'Removing Istio label failed')
                    }    

}