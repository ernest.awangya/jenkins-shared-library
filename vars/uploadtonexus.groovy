def call(NEXUSIP, NEXUSPORT, RELEASE_REPO, NEXUS_LOGIN) {
    stage("Upload Artifact to Nexus") {
        steps {
            script {
                nexusArtifactUploader(
                    nexusVersion: 'nexus3',
                    protocol: 'http',
                    nexusUrl: "${NEXUSIP}:${NEXUSPORT}",
                    groupId: 'QA',
                    version: "${env.BUILD_ID}-${env.BUILD_TIMESTAMP}",
                    repository: "${RELEASE_REPO}",
                    credentialsId: "${NEXUS_LOGIN}",
                    artifacts: [
                        [artifactId: 'demoapp',
                            classifier: '',
                            file: 'target/demoapp-v2.war',
                            type: 'war']
                    ]
                )
            }
        }
    }
}