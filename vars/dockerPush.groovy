def call(){
    withCredentials([usernamePassword(
        credentialsId: 'dockerhub-creds', 
        passwordVariable: 'PASS', 
        usernameVariable: 'USER'
        )]) {
        
        sh "echo \${PASS} | docker login -u \${USER} --password-stdin"
        }
        sh "docker push eawangya/techharbor:version2"
        sh "docker rmi -f \$(docker images -qa)"
}
