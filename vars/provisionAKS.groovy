def call(){

    dir('terraform') {
        sh 'terraform init'
        sh 'terraform validate'
        sh 'terraform destroy --auto-approve'
    }

}