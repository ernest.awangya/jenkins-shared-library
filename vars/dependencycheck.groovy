def call(){
    dependencyCheck additionalArguments: '', odcInstallation: 'DP-check'
    dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
}