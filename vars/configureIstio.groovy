def call(){
    dir('kubenetes-files') {
        sh "az aks get-credentials --resource-group my-demo-rg --name my-demo-cluster --admin --overwrite-existing"
          }
        sh "curl -L https://istio.io/downloadIstio | ISTIO_VERSION=${ISTIO_VERSION} sh -"
        
    dir("istio-1.20.0") {
        sh 'export PATH="$PATH:/var/lib/jenkins/workspace/profile-project/istio-1.20.0/bin"'
        sh '${ISTIO_PATH}/istioctl install --set profile=demo -y'
        sh 'kubectl label namespace default istio-injection=enabled'
    }
}