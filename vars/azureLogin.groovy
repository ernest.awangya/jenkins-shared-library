def call(){
    withCredentials([azureServicePrincipal(
        credentialsId: 'AzureServicePrincipal', 
        variable: 'AZURE_CREDENTIALS'
        )]) {
        sh "az login --service-principal --username $AZURE_CREDENTIALS_CLIENT_ID --password $AZURE_CREDENTIALS_CLIENT_SECRET --tenant $AZURE_CREDENTIALS_TENANT_ID"
        }
}